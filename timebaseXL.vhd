-- Date: 15 May 2017
-- Authors
-- Sebastian Jordan, student number: 4590813
-- Mathijs van Geerenstein, student number: 4598660

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity timebaseXL is
	port (	clk		: in	std_logic;
		reset		: in	std_logic;
		count_RS : in std_logic;
		count_out	: out	std_logic_vector (5 downto 0) -- 20 bits needed to count to 1000000
	);
end entity timebaseXL;

architecture behavioural of timebaseXL is
  signal count, new_count  : unsigned (5 downto 0);
  
begin
  process (clk)
    begin
      if (clk'event and clk = '1') then -- Act on rising edge
        if (reset = '1') then
          count <= (others => '0'); -- Count value is reset after a reset signal
		  else
          count <= new_count;
        end if;
      end if;
    end process;
    
  process (count, count_RS)
    begin
	 if (count_RS = '1') then
     new_count <= count+1; 
	  else
	  new_count <= count;
	  end if;
	end process;
    
    count_out <= std_logic_vector(count);
end architecture behavioural;
        