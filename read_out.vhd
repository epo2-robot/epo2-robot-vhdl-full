library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity sensor_readout is
	port ( sensor		: in	std_logic;
		clk		: in	std_logic;
		reset		:in	std_logic;
		mine_detected	: out	std_logic
		);
end entity sensor_readout;

architecture behavioural of sensor_readout is

type sensor_readout_state is ( reset_state , count_reset , count , compare, detected ) ;
  
signal counter, new_counter: unsigned (13 downto 0) := to_unsigned (0 , 14) ;
constant counted : unsigned (13 downto 0) := to_unsigned (5195 , 14 ) ; -- constante nog ff tweaken
signal state, next_state :sensor_readout_state ;

begin

process (clk)
	begin
		if ( rising_edge(clk)) then
			if ( reset = '1' ) then 
				state <= reset_state ;
				counter <= (others => '0') ;
			else
				state <= next_state ;
				counter <= new_counter ;
			end if ;
		end if ;
	end process ;

process ( state , counter ,sensor ) 
begin
case state is
			when reset_state =>
				mine_detected <= '0' ;
				new_counter <= counter ; 
				if(sensor = '0') then
					next_state <= count_reset ;
				else 
					next_state <= reset_state;
				end if ;
			when count_reset =>
				mine_detected <= '0' ;
				new_counter <= (others => '0') ;
				if (sensor = '1' ) then
					next_state <= count ;
				else
					next_state <= count_reset ;
				end if ;
						
			when count =>
				mine_detected <= '0' ;
				new_counter <= counter + 1 ;
				if(sensor = '0') then
					next_state <= compare ;
				else 
					next_state <= count ;
				end if ;
			when compare =>
				mine_detected <= '0' ;
				new_counter <= counter  ;
				if ( counter < counted ) then
					next_state <= count_reset ;
				else
					next_state <= detected ;
				end if ;

			when detected =>
				mine_detected <= '1' ;
				new_counter <= counter  ;
				next_state <= detected ;
		end case ;
end process ;
end architecture behavioural ;
