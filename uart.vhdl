library ieee;
use ieee.std_logic_1164.all;

entity uart is
	port (
		clk, reset: in std_logic;
		rx_u: in std_logic; --input bit stream
		tx_u: out std_logic; --output bit stream
		data_send : in std_logic_vector(7 downto 0); --byte to be sent
		data_receive : out std_logic_vector(7 downto 0); --received byte
		write_data: in std_logic; --write to transmitter buffer 
		read_data: in std_logic; --read from receiver buffer 
		rx_buffer_flag : out std_logic
		
	);
end uart;

architecture structural of uart is 

component baud_gen is
   generic ( M : integer := 326  ); -- baud rate divisor M = 50M/(16*9600)
   port(
      clk , reset : in std_logic ;
      s_tick : out std_logic  );
end component baud_gen ;

component uart_rx is
   port(
      clk, reset: in std_logic;
      rx: in std_logic; -- icoming serial bit stream
      s_tick: in std_logic; -- sampling tick from baud rate generator
      rx_done_tick: out std_logic; -- data frame completion tick
      dout: out std_logic_vector(7 downto 0) -- data byte
   );
end component uart_rx ;

component uart_tx is
   port(
      clk, reset: in std_logic;
      tx_start: in std_logic; -- if '1' transmission starts
      s_tick: in std_logic; -- sampling tick from baud rate generator
      din: in std_logic_vector(7 downto 0); -- incoming data byte
      tx_done_tick: out std_logic; -- data frame completion tick 
      tx: out std_logic -- outcoming bit stream
   );
end component uart_tx ;

component buf_reg is
   port(
      clk, reset: in std_logic;
      clr_flag, set_flag: in std_logic; 
      din: in std_logic_vector(7 downto 0);
      dout: out std_logic_vector(7 downto 0);
      flag: out std_logic
   );
end component buf_reg;

signal s_tick_tussen , rx_done_tick_tussen , tx_done_tick_tussen , tx_buffer_flag 	: std_logic ;
signal rx_dout	, dout_buffer_tx		 : std_logic_vector(7 downto 0) ;

begin 

L1: 	 baud_gen  port map ( clk => clk , reset => reset , s_tick => s_tick_tussen ) ;

L2:	 uart_rx   port map ( clk => clk , reset => reset , rx => rx_u , s_tick => s_tick_tussen , dout => rx_dout , rx_done_tick => rx_done_tick_tussen );

L3: 	 uart_tx   port map ( clk => clk , reset => reset , tx => tx_u , s_tick => s_tick_tussen , tx_done_tick => tx_done_tick_tussen , din => dout_buffer_tx , tx_start => tx_buffer_flag );

L4_rx:  buf_reg   port map ( clk => clk , reset => reset , din => rx_dout , set_flag => rx_done_tick_tussen  , clr_flag => read_data , flag => rx_buffer_flag , dout => data_receive );

L4_tx:  buf_reg   port map ( clk => clk , reset => reset , din => data_send, set_flag => write_data  , clr_flag => tx_done_tick_tussen , flag => tx_buffer_flag , dout => dout_buffer_tx  );

end architecture structural ;
