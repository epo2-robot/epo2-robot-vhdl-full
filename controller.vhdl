
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity controller is
	port (	clk			: in	std_logic;
		reset			: in	std_logic;

		sensor_l		: in	std_logic;
		sensor_m		: in	std_logic;
		sensor_r		: in	std_logic;
		mine_sensor		: in	std_logic;

		data_receive 		: in	std_logic_vector(7 downto 0) ;
		flag			: in	std_logic ;

		count_in		: in	std_logic_vector (19 downto 0);
		count_reset		: out	std_logic;
		
		count2_in		: in	std_logic_vector (5 downto 0);
		count2_reset		: out	std_logic;

		motor_l_reset		: out	std_logic;
		motor_l_direction	: out	std_logic;

		motor_r_reset		: out	std_logic;
		motor_r_direction	: out	std_logic;

		data_send		: out	std_logic_vector (7 downto 0);
		write_data		: out	std_logic ;
		read_data		: out	std_logic 
	);
end entity controller;


architecture behavioral of controller is 

type controller_state is ( reset_state , line_state , gentle_R , gentle_L , sharp_R , sharp_L, sharp_L_2 , sharp_R_2  , tussen_left , tussen_right , tussen_line ,
			     reset_left , reset_right , stop_state, send_state  , all_black ,  all_blackRS , back_state, tussen_back_state,send_mine, back_reset,
				Uturn1, Uturn1Rs, Uturn2, Uturn2Rs , sharp_L_3 , sharp_R_3 , reset_left_2 , reset_right_2 , com_error) ;

signal state , next_state : controller_state ;
signal  T : unsigned (19 downto 0) ;
constant T1 : unsigned (19 downto 0) := to_unsigned ( 999998 , 20 ) ; 

signal Tb: unsigned (5 downto 0);
constant T2 : unsigned (5 downto 0) := to_unsigned (17, 6); -- Count needed to get wheels on line at crossing



begin 
	T <= unsigned ( count_in ) ;
	 Tb <= unsigned (count2_in);

	process (clk )
	begin
		
		if ( rising_edge(clk)) then
			if ( reset = '1' ) then  
				state <= stop_state ;
			else
				state <= next_state ;
			end if ;
		end if ;
	end process ;


	process ( state , T ,sensor_l , sensor_m , sensor_r , flag , data_receive , Tb , mine_sensor)
	begin
		case state is

			when reset_state =>

				motor_l_reset <= '1' ;
				motor_r_reset <= '1' ;
				count_reset   <= '1' ;

				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

			if ( mine_sensor = '1' ) then
				next_state <= tussen_back_state ;
			else				
				
				 if ( (sensor_l = '1' ) and ( sensor_m = '0' ) and (sensor_r = '1')  ) then
					next_state <=  line_state ;
				elsif ( (sensor_l = '0' ) and ( sensor_m = '1' ) and (sensor_r = '1')  ) then
					next_state <=  sharp_L ;
				elsif ( (sensor_l = '1' ) and ( sensor_m = '1' ) and (sensor_r = '0')  ) then
					next_state <=  sharp_R ;
				elsif ( (sensor_l = '0' ) and ( sensor_m = '0' ) and (sensor_r = '1')  ) then
					next_state <=  gentle_L ;
				elsif ( (sensor_l = '1' ) and ( sensor_m = '0' ) and (sensor_r = '0')  ) then
					next_state <=  gentle_R ;
				elsif ( (sensor_l = '0' ) and ( sensor_m = '1' ) and (sensor_r = '0')  ) then
					next_state <=  line_state ;
				elsif ( (sensor_l = '1' ) and ( sensor_m = '1' ) and (sensor_r = '1')  ) then
					if ( (data_receive = "01110011")  ) then -- s
						next_state <=  stop_state ;	
					else				
						next_state <=  sharp_L ;
					end if ;
				elsif ( (sensor_l = '0' ) and ( sensor_m = '0' ) and (sensor_r = '0')  ) then
					next_state <=  all_black ;
				else
					next_state <=  line_state ;
					
				end if ;
			end if ;


			when line_state => 
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '1' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

				if (  T < T1  ) then
					next_state <= line_state ;
				else 
					next_state <= reset_state ;
				end if;

			when gentle_R =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '1' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '1' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;


				if (  T < T1  ) then
					next_state <= gentle_R ;
				else
					next_state <= reset_state ;
				end if;

			when gentle_L =>
				motor_l_reset <= '1' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;


				if (  T < T1  ) then
					next_state <= gentle_L ;
				else
					next_state <= reset_state ;
				end if;	
	
			when sharp_R =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '1' ;
				motor_r_direction <= '1' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;


				if (  T < T1  ) then
					next_state <= sharp_R ;
				else
					next_state <= reset_state ;
				end if;	
	
			when sharp_L =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;


				if (  T < T1  ) then
					next_state <= sharp_L ;
				else
					next_state <= reset_state ;
				end if;		

			when all_black => 
     				 motor_l_reset <= '0';
    				 motor_r_reset <= '0';
     				 count_reset <= '0';
      				 count2_reset <= '0';
     				 motor_l_direction <= '1';
     				 motor_r_direction <= '0';

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;
       
      			 if (T < T1) then
		 		  next_state  <= all_black;
     			 else 
      				  next_state  <= all_blackRS;
      			 end if;

			 when all_blackRS =>
     				  motor_l_reset <= '1';
     				  motor_r_reset <= '1';
     				  count_reset <= '1';
     				  count2_reset <= '0';
     				  motor_l_direction <= '0';
      				  motor_r_direction <= '0';

				  data_send  <=  ( others => '0' ) ;
				  write_data <=  '0' ;
				  read_data  <=  '0' ;
 
      		 if ( Tb <= T2 ) then 
       			 next_state <= all_black;
		 else
			if ( flag = '1' ) then
				if (  data_receive = "01100110" ) then -- f
					next_state  <= tussen_line ;
				elsif (  data_receive = "01101100" ) then --l
					next_state <= Sharp_L_2 ;
				elsif (  data_receive = "01110010" ) then --r
					next_state <= Sharp_R_2 ;
				else 
					next_state  <= com_error  ;
				end if ;
			else
				next_state  <= all_black  ;
      			 end if ;  
		end if ; 

			when tussen_line => -- Forward --
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '1' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;
			
				if (  T < T1  ) then
					next_state <= tussen_line ;
				else
					next_state <= send_state ;
				end if;

			

			when sharp_L_2 =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;
			
				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

				if (  T < T1  ) then
					next_state <= sharp_L_2 ;
				else
					next_state <= reset_left ;
				end if;		


			when reset_left => 
				motor_l_reset <= '1' ;
				motor_r_reset <= '1' ;
				count_reset   <= '1' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

			
				if (sensor_l = '1' and sensor_m = '1' and sensor_r = '1') then
      					 next_state <= sharp_L_3;
    				else
     					  next_state <= sharp_L_2;
				end if;

			when sharp_L_3 =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;
			
				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

				if (  T < T1  ) then
					next_state <= sharp_L_3 ;
				else
					next_state <= reset_left_2 ;
				end if;	

			when reset_left_2 => 
				motor_l_reset <= '1' ;
				motor_r_reset <= '1' ;
				count_reset   <= '1' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

			
				if (not(sensor_l = '1' and sensor_m = '1' and sensor_r = '1')) then
      					 next_state <= tussen_left;
    				else
     					  next_state <= sharp_L_3;
				end if;	

			when tussen_left => -- Gentle left --
				motor_l_reset <= '1' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;


				if (  T < T1  ) then
					next_state <= tussen_left ;
				else
					next_state <= send_state ;
				end if;	



			when sharp_R_2 =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '1' ;
				motor_r_direction <= '1' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

				if (  T < T1  ) then
					next_state <= sharp_R_2 ;
				else
					next_state <= reset_right ;
				end if;	

			when reset_right => 
				motor_l_reset <= '1' ;
				motor_r_reset <= '1' ;
				count_reset   <= '1' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

			
				if (sensor_l = '1' and sensor_m = '1' and sensor_r = '1') then
    				   next_state <= sharp_R_3;
   				  else
     				  next_state <= sharp_R_2 ;
				end if ;

			when sharp_R_3 =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '1' ;
				motor_r_direction <= '1' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

				if (  T < T1  ) then
					next_state <= sharp_R_3 ;
				else
					next_state <= reset_right_2 ;
				end if;	

			when reset_right_2 => 
				motor_l_reset <= '1' ;
				motor_r_reset <= '1' ;
				count_reset   <= '1' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

			
				  if (not(sensor_l = '1' and sensor_m = '1' and sensor_r = '1')) then
    				   next_state <= tussen_right;
   				  else
     				  next_state <= sharp_R_3 ;
				end if ;

			
			when tussen_right => -- Gentle Right --
				motor_l_reset <= '0' ;
				motor_r_reset <= '1' ;
				count_reset   <= '0' ;
				count2_reset <= '1';

				motor_l_direction <= '1' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

				if (  T < T1  ) then
					next_state <= tussen_right ;
				else
					next_state <= send_state ;
				end if;	

			

			when send_state => 
				motor_l_reset <= '1' ;
				motor_r_reset <= '1' ;
				count_reset   <= '1' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				read_data  <=  '1' ;
				data_send  <=  "01101011" ; -- K
				write_data <=  '1' ;
				
				
			
				if    ( (sensor_l = '0' ) and ( sensor_m = '1' ) and (sensor_r = '1')  ) then
					next_state <=  sharp_L ;
				elsif ( (sensor_l = '1' ) and ( sensor_m = '1' ) and (sensor_r = '0')  ) then
					next_state <=  sharp_R ;
				elsif ( (sensor_l = '0' ) and ( sensor_m = '0' ) and (sensor_r = '1')  ) then
					next_state <=  gentle_L ;
				elsif ( (sensor_l = '1' ) and ( sensor_m = '0' ) and (sensor_r = '0')  ) then
					next_state <=  gentle_R ;
				elsif ( (sensor_l = '1' ) and ( sensor_m = '1' ) and (sensor_r = '1')  ) then
					next_state <=  sharp_L ;
				else 
					next_state <=  line_state ;
		
				end if ;

			when tussen_back_state => 
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '0';

				motor_l_direction <= '0' ;
				motor_r_direction <= '1' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

				if (  T < T1  ) then
					next_state <= tussen_back_state ;
				else 
					next_state <= send_mine;
				end if;

			when send_mine => 
				motor_l_reset <= '1' ;
				motor_r_reset <= '1' ;
				count_reset   <= '1' ;
				count2_reset <= '0';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				read_data  <=  '1' ;
				data_send  <=  "01101101" ; -- m
				write_data <=  '1' ;

				next_state <= back_state ;

			when back_state => 
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;
				count2_reset <= '0';

				motor_l_direction <= '0' ;
				motor_r_direction <= '1' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

				if (  T < T1  ) then
					next_state <= back_state ;
				else 
					next_state <= back_reset;
				end if;
				

			when back_reset =>
     				  motor_l_reset <= '1';
     				  motor_r_reset <= '1';
     				  count_reset <= '1';
     				  count2_reset <= '0';
     				  motor_l_direction <= '0';
      				  motor_r_direction <= '0';

				  data_send  <=  ( others => '0' ) ;
				  write_data <=  '0' ;
				  read_data  <=  '0' ;
 
      		 if ( Tb <= T2 ) then 
       			next_state <= back_state;
		 else
			if ( flag = '1' ) then

				if (  data_receive = "01110101" ) then -- u
					next_state  <= Uturn1 ;
				elsif (  data_receive = "01101100" ) then --l
					next_state <= Sharp_L_2 ;
				elsif (  data_receive = "01110010" ) then --r
					next_state <= Sharp_R_2 ;
				else 
					next_state  <= com_error ;
				end if ;
			else
				next_state  <= back_state ;
			end if ;
      		 end if;  

			when uturn1 =>
      				motor_l_reset <= '0';
     				motor_r_reset <= '0';
      				count_reset <= '0';
		 		count2_reset <= '1';
       				motor_l_direction <= '1';
      				motor_r_direction <= '1';

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;
     				  if (T < T1) then
         				next_state <= uturn1;
      				 else
        				 next_state <= uturn1RS;
       				end if; 

			 when uturn1RS =>
				 motor_l_reset <= '1';
    				 motor_r_reset <= '1';
    				 count_reset <= '1';
	 			 count2_reset <= '1';
    				 motor_l_direction <= '1';
    				 motor_r_direction <= '1';

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

   			  if (sensor_l = '1' and sensor_m = '1' and sensor_r = '1') then
     				  next_state <= uturn2;
     			 else
      				 next_state <= uturn1;
    			 end if;  

			 when uturn2 =>
     				  motor_l_reset <= '0';
     				  motor_r_reset <= '0';
      				 count_reset <= '0';
				 count2_reset <= '1';
      				 motor_l_direction <= '1';
      				 motor_r_direction <= '1';

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

      			 if (T < T1) then
        			 next_state <= uturn2;
      			 else
        			 next_state <= uturn2RS;
       			end if;   

			 when uturn2RS =>
				motor_l_reset <= '1';
     				motor_r_reset <= '1';
    				 count_reset <= '1';
	 			 count2_reset <= '1';
    				 motor_l_direction <= '1';
    				 motor_r_direction <= '1';

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;


   			  if (not(sensor_l = '1' and sensor_m = '1' and sensor_r = '1')) then
     				  next_state <= sharp_R_2;
    			 else
   			   	 next_state <= uturn2;
    			 end if;    

			when stop_state =>
				motor_l_reset <= '1' ;
				motor_r_reset <= '1' ;
				count_reset   <= '1' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				data_send  <=  ( others => '0' ) ;
				write_data <=  '0' ;
				read_data  <=  '0' ;

			if ( flag = '1' ) then 
				if ( data_receive = "01100010" ) then -- b begin
					next_state <=  send_state ;
				elsif (  data_receive = "01100110" ) then -- f forward
					next_state  <= tussen_line ;
				elsif (  data_receive = "01101100" ) then --l left
					next_state <= Sharp_L_2 ;
				elsif (  data_receive = "01110010" ) then --r right
					next_state <= Sharp_R_2 ;
				elsif (  data_receive = "01110101" ) then -- u uturn
					next_state  <= Uturn1 ;
				elsif ( data_receive = "01110011" ) then --s stop
					next_state <= stop_state ;
				else
					next_state <= com_error ; -- communication error
				end if ;
			else
				next_state <=  stop_state ;
			end if ;


			when com_error =>
				motor_l_reset <= '1' ;
				motor_r_reset <= '1' ;
				count_reset   <= '1' ;
				count2_reset <= '1';

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				read_data  <=  '1' ;
				data_send  <=  "01111000" ; -- x
				write_data <=  '1' ;

			
				next_state <=  stop_state ;
			
			

		end case ;
	end process ;
end architecture behavioral ;