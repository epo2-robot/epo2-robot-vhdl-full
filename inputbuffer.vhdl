--Authors: 
--Mohammed Abo Alainein   4570081
--Nick Hogendoorn         4567870


library IEEE;
use IEEE.std_logic_1164.all;


entity inputbuffer is
	port (	clk		: in	std_logic;

		sensor_l_in	: in	std_logic;
		sensor_m_in	: in	std_logic;
		sensor_r_in	: in	std_logic;

		sensor_l_out	: out	std_logic;
		sensor_m_out	: out	std_logic;
		sensor_r_out	: out	std_logic
	);
end entity inputbuffer;


architecture behavioral of inputbuffer is

signal sensor_l_tussen , sensor_m_tussen , sensor_r_tussen : std_logic; -- signals to connect the output of the first flipflop with the input of the second flipflop 

begin
   -- This process generates the first flipflop 
	process (clk)
	begin
		if ( rising_edge (clk) ) then
			
			sensor_l_tussen <= sensor_l_in ;
			sensor_m_tussen <= sensor_m_in ;
			sensor_r_tussen <=  sensor_r_in ;
 			
		 end if ;
	end process ;

	process ( clk )
	   -- This process generates the second flipflop 
	begin
		if ( rising_edge (clk) ) then
			sensor_l_out <= sensor_l_tussen ;
			sensor_m_out <= sensor_m_tussen ;
			sensor_r_out <= sensor_r_tussen ;
		end if ;

	end process ;

end architecture behavioral ;
	
			
