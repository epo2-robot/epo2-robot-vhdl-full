--Authors: 
--Mohammed Abo Alainein   4570081
--Nick Hogendoorn         4567870


library IEEE;
use IEEE.numeric_std.all;
use IEEE.std_logic_1164.all;

entity motorcontrol is
	port (	clk		    		 : in std_logic;
		     reset		   	 : in	std_logic;
		     direction	 		 : in	std_logic;
		     count_in	 		 : in	std_logic_vector (19 downto 0); 
		     pwm		     	 : out	std_logic
	     );
end entity motorcontrol;

architecture behavioral of motorcontrol is 

type motor_controller_state is ( reset_state , pwm_high , pwm_low ) ; -- 3 states are needed ---

signal present_state , next_state :  motor_controller_state ;
signal  T : unsigned (19 downto 0) ; -- signal used to read the counter value as unsigned which easier for comparing --
constant T1 : unsigned (19 downto 0) := to_unsigned (50000 , 20 ) ; -- constant used to compare the counter signal with 1 ms	(mutor turns left )
constant T2 : unsigned (19 downto 0) :=  to_unsigned (100000 , 20 ) ; -- constant used to compare the counter signal with 2 ms  ( motor turns rights )

begin 

  T <= unsigned ( count_in )  ; 

  -- This process generates the state transition
	process (clk)
	begin
		if ( rising_edge(clk)) then
			if ( reset = '1' ) then 
				present_state <= reset_state ;
			else
				present_state <= next_state ;
			end if ;
		end if ;
	end process ;

  -- This process implements the FSM
	process ( present_state , T , direction )
	begin
		case present_state is
			when reset_state =>
				pwm <= '0' ; 
				next_state <=  pwm_high ;

			when pwm_high =>
				pwm <= '1' ;
				if ( (direction = '0') and ( T >= T1 ) ) then -- when the counter reaches 1 ms go to Pwm_low state
					next_state <= pwm_low ;
				elsif ( (direction = '1') and ( T >= T2 ) ) then -- when the counter reaches 1 ms go to Pwm_low state
					next_state <= pwm_low ;
				else
					 next_state <=  pwm_high ;
				end if;
						
			when pwm_low =>
				pwm <= '0' ;
				next_state <=  pwm_low ;

		end case ;
	end process ;
end architecture behavioral ;
				
			

