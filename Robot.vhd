


		---- Top Level -----

library IEEE;
use IEEE.std_logic_1164.all;

entity Robot_top_level is
port (	        clk		: in	std_logic;
		reset 		: in	std_logic;

		sensor_l_in	: in	std_logic;
		sensor_m_in	: in	std_logic;
		sensor_r_in	: in	std_logic;
		sensor_mine	: in	std_logic;
		
		rx_in		: in	std_logic;
		tx_out		: out	std_logic;
		pwmL		: out	std_logic;
		pwmR		: out	std_logic
	);
end entity Robot_top_level ;

architecture structural of Robot_top_level is

component inputbuffer is
	port (	clk		: in	std_logic;

		sensor_l_in	: in	std_logic;
		sensor_m_in	: in	std_logic;
		sensor_r_in	: in	std_logic;

		sensor_l_out	: out	std_logic;
		sensor_m_out	: out	std_logic;
		sensor_r_out	: out	std_logic
	);
end component inputbuffer;

component controller is
	port (	clk			: in	std_logic;
		reset			: in	std_logic;

		sensor_l		: in	std_logic;
		sensor_m		: in	std_logic;
		sensor_r		: in	std_logic;
		mine_sensor		: in	std_logic;

		data_receive 		: in	std_logic_vector(7 downto 0) ;
		flag			: in	std_logic ;

		count_in		: in	std_logic_vector (19 downto 0);
		count_reset		: out	std_logic;
	
		count2_in		: in	std_logic_vector (5 downto 0);
		count2_reset		: out	std_logic;

		motor_l_reset		: out	std_logic;
		motor_l_direction	: out	std_logic;

		motor_r_reset		: out	std_logic;
		motor_r_direction	: out	std_logic;

		data_send		: out	std_logic_vector (7 downto 0);
		write_data		: out	std_logic ;
		read_data		: out	std_logic 
	);
end component controller;

component timebase is
	port (	clk		: in	std_logic;
		reset		: in	std_logic;

		count_out	: out	std_logic_vector (19 downto 0)
	);
end component timebase;

component timebaseXL is
	port (	clk 		: in std_logic;
		count_RS	: in	std_logic;
		reset		: in	std_logic;

		count_out	: out	std_logic_vector (5 downto 0) 
	);
end component timebaseXL;

component motorcontrol is
	port (	clk		: in	std_logic;
		reset		: in	std_logic;
		direction	: in	std_logic;
		count_in	: in	std_logic_vector (19 downto 0);

		pwm		: out	std_logic
	);
end component motorcontrol;

component uart is
	port (
		clk, reset: in std_logic;
		rx_u: in std_logic; --input bit stream
		tx_u: out std_logic; --output bit stream
		data_send : in std_logic_vector(7 downto 0); --byte to be sent
		data_receive : out std_logic_vector(7 downto 0); --received byte
		write_data: in std_logic; --write to transmitter buffer 
		read_data: in std_logic; --read from receiver buffer 
		rx_buffer_flag : out std_logic
		
	);
end component uart;
component sensor_readout is
	port ( sensor		: in	std_logic;
		clk		: in	std_logic;
		reset		: in	std_logic;
		mine_detected	: out	std_logic
		);
end component sensor_readout;

signal sensorL,sensorM,sensorR, countRS , count2RS , M_L_RS, M_L_D, M_R_RS, M_R_D : std_logic;
signal count : std_logic_vector (19 downto 0);
signal data_receive_tussen , data_send_tussen 	: std_logic_vector(7 downto 0);
signal write_data_tussen , read_data_tussen , rx_buffer_flag_tussen	:std_logic;
signal count2 : std_logic_vector (5 downto 0);
signal mine: std_logic;

begin 

LB1: inputbuffer  port map ( clk => clk, sensor_l_in => sensor_l_in , sensor_m_in => sensor_m_in  ,sensor_r_in => sensor_r_in,
			    sensor_l_out => sensorL , sensor_m_out => sensorM , sensor_r_out => sensorR		    );

LB2: controller   port map ( clk => clk , reset => reset , sensor_l => sensorL , sensor_m => sensorM , sensor_r => sensorR , count_in => count , count_reset=>countRS ,count2_in => count2,
			     motor_l_reset => M_L_RS , motor_l_direction =>  M_L_D , motor_r_reset => M_R_RS , motor_r_direction => M_R_D, flag => rx_buffer_flag_tussen,mine_sensor=>mine,
			     data_receive => data_receive_tussen , data_send => data_send_tussen , write_data => write_data_tussen , read_data=> read_data_tussen , count2_reset => count2RS);

LB3: timebase  	  port map ( clk => clk , reset => countRS , count_out => count );

MCL: motorcontrol port map ( clk => clk , reset => M_L_RS , direction => M_L_D , count_in => count , pwm=>pwmL ) ;

MCR: motorcontrol port map ( clk => clk , reset => M_R_RS , direction => M_R_D , count_in => count , pwm=>pwmR ) ;

LB4: timebaseXL   port map (clk => clk, reset => count2RS, count_RS => countRS, count_out => count2);

LB5: uart	  port map ( clk => clk , reset => reset , rx_u => rx_in , tx_u => tx_out , data_receive => data_receive_tussen , data_send => data_send_tussen ,
			     write_data => write_data_tussen , read_data => read_data_tussen , rx_buffer_flag => rx_buffer_flag_tussen	);
LB6: sensor_readout port map ( clk => clk , reset => countRS, sensor => sensor_mine , mine_detected => mine ) ;

end architecture structural ;

